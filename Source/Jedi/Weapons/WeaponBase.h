// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WeaponBase.generated.h"

UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	WT_Lightsaber UMETA(DisplayName="Lightsaber"),
	WT_Pistol UMETA(DisplayName="Pistol"),
	WT_AssaultRifle UMETA(DisplayName="AssaultRifle"),
	WT_SniperRifle UMETA(DisplayName="SniperRifle")
};


UCLASS()
class JEDI_API AWeaponBase : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
	USceneComponent* RootSceneComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Components")
	class USkeletalMeshComponent* WeaponMesh;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName AttachHandsSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FName AttachSocketName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	EWeaponType WeaponType;
	
public:
	AWeaponBase();

protected:
	virtual void BeginPlay() override;
	
};
