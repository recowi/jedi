// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Jedi/Weapons/WeaponBase.h"

#include "LightsaberBase.generated.h"


DECLARE_DYNAMIC_MULTICAST_DELEGATE(FUpdateBladeColor);


UCLASS()
class JEDI_API ALightsaberBase : public AWeaponBase
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, ReplicatedUsing=OnRep_BladeColor)
	FLinearColor BladeColor;
	
	UPROPERTY(EditDefaultsOnly, Replicated)
    UChildActorComponent* FirstBlade;

	UPROPERTY(EditDefaultsOnly, Replicated)
	UChildActorComponent* SecondBlade;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UAudioComponent* AudioComponent;

	UPROPERTY()
	FUpdateBladeColor UpdateBladeColor;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USoundBase* IdleSound;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USoundBase* StartBladeSound;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	USoundBase* StopBladeSound;
	
	
public:
	ALightsaberBase();

protected:
	virtual void BeginPlay() override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	void ToggleFirstBlade();
	void ToggleSecondBlade();

	UFUNCTION()
	void OnRep_BladeColor();

	void InitAudioComponent();

	void TurnOffBlades();

private:
	UFUNCTION(NetMulticast, Reliable)
	void ToggleAudioComponent(bool bIsTurn);
	
	void InitLightsaber();
};
