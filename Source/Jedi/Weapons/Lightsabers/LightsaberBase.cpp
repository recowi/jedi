// Fill out your copyright notice in the Description page of Project Settings.


#include "LightsaberBase.h"


#include "BladeBase.h"
#include "Components/AudioComponent.h"
#include "Net/UnrealNetwork.h"


ALightsaberBase::ALightsaberBase()
{
	AttachSocketName = "LightsaberSocket";

	FirstBlade = CreateDefaultSubobject<UChildActorComponent>("FirstBlade");
	FirstBlade->SetupAttachment(WeaponMesh);
	
	SecondBlade = CreateDefaultSubobject<UChildActorComponent>("SecondBlade");
	SecondBlade->SetupAttachment(WeaponMesh);

	AudioComponent = CreateDefaultSubobject<UAudioComponent>("AudioComponent");
	AudioComponent->SetupAttachment(WeaponMesh);
	
}

void ALightsaberBase::BeginPlay()
{
	Super::BeginPlay();
	
	InitLightsaber();
}

void ALightsaberBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ALightsaberBase, FirstBlade);
	DOREPLIFETIME(ALightsaberBase, SecondBlade);
	DOREPLIFETIME(ALightsaberBase, BladeColor);
}

void ALightsaberBase::ToggleFirstBlade()
{
	if(ABladeBase* Blade = Cast<ABladeBase>(FirstBlade->GetChildActor()))
	{
		switch (Blade->BladeState)
		{
			case EBladeState::BS_BladeOff:
				{
					ToggleAudioComponent(true);
					Blade->ToggleBlade();
				}
				break;
			case EBladeState::BS_BladeOn:
				{	
					if(ABladeBase* OtherBlade = Cast<ABladeBase>(SecondBlade->GetChildActor()))
					{
						if(OtherBlade->BladeState == EBladeState::BS_BladeOn)
						{
							OtherBlade->ToggleBlade();
							break;
						}
					}

					ToggleAudioComponent(false);
					Blade->ToggleBlade();
				}
				break;
			default:
				{
					
				}
				break;
		}
	}
}

void ALightsaberBase::ToggleSecondBlade()
{
	if(ABladeBase* Blade = Cast<ABladeBase>(SecondBlade->GetChildActor()))
	{
		switch (Blade->BladeState)
		{
		case EBladeState::BS_BladeOff:
			{
				Blade->ToggleBlade();

				if(auto OtherBlade = Cast<ABladeBase>(FirstBlade->GetChildActor()))
				{
					if(OtherBlade->BladeState == EBladeState::BS_BladeOff)
					{
						ToggleAudioComponent(true);
						OtherBlade->ToggleBlade();
					}
				}
			}
			break;
		case EBladeState::BS_BladeOn:
			{	
				Blade->ToggleBlade();

				if(auto OtherBlade = Cast<ABladeBase>(FirstBlade->GetChildActor()))
				{
					if(OtherBlade->BladeState == EBladeState::BS_BladeOn)
					{
						ToggleAudioComponent(false);
						OtherBlade->ToggleBlade();
					}
				}
			}
			break;
		default:
			{
					
			}
			break;
		}
	}
}

void ALightsaberBase::OnRep_BladeColor()
{
	UpdateBladeColor.Broadcast();
}

void ALightsaberBase::InitAudioComponent()
{
	if(ABladeBase* Blade = Cast<ABladeBase>(FirstBlade->GetChildActor()))
	{
		if(Blade->BladeState == EBladeState::BS_BladeOff)
		{
			AudioComponent->FadeOut(0.1, 0);
		}
		else
		{
			AudioComponent->FadeIn(0.1f);
		}
	}
}

void ALightsaberBase::TurnOffBlades()
{
	if(ABladeBase* Blade = Cast<ABladeBase>(SecondBlade->GetChildActor()))
	{
		if(Blade->BladeState == EBladeState::BS_BladeOn)
		{
			Blade->ToggleBlade();
		}
	}

	if(ABladeBase* Blade = Cast<ABladeBase>(FirstBlade->GetChildActor()))
	{
		if(Blade->BladeState == EBladeState::BS_BladeOn)
		{
			ToggleAudioComponent(false);
			Blade->ToggleBlade();
		}
	}
}

void ALightsaberBase::InitLightsaber()
{
	AudioComponent->Sound = IdleSound;
	
	InitAudioComponent();
}

void ALightsaberBase::ToggleAudioComponent_Implementation(bool bIsTurn)
{
	if(bIsTurn)
	{
		AudioComponent->FadeIn(1.f);
	}
	else
	{
		AudioComponent->FadeOut(1.f, 0);	
	}
}
