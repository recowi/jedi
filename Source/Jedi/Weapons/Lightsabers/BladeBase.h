// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BladeBase.generated.h"

UENUM(BlueprintType)
enum class EBladeState : uint8
{
	BS_BladeOn UMETA(DisplayName="BladeOn"),
	BS_BladeOff UMETA(DisplayName="BladeOff")
};

UCLASS(Blueprintable)
class JEDI_API ABladeBase : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class USkeletalMeshComponent* BladeMesh;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	class UPointLightComponent* PointLightComponent;

	UPROPERTY(EditDefaultsOnly, Replicated, BlueprintReadWrite)
	EBladeState BladeState;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxIntensityPointLight;
	
private:
	FTimerHandle InitHandler;

public:	
	ABladeBase();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;


public:
	UFUNCTION(BlueprintImplementableEvent)
	void ToggleBlade();
	
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
    void UpdateBladeAnimation(bool bIsTurnOn);

private:
	UFUNCTION()
	void UpdateBladeColor();
	
	void InitBlade();
	
};
