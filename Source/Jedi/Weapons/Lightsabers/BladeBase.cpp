// Fill out your copyright notice in the Description page of Project Settings.


#include "BladeBase.h"



#include "LightsaberBase.h"
#include "Components/PointLightComponent.h"
#include "Net/UnrealNetwork.h"

ABladeBase::ABladeBase()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;
	
	MaxIntensityPointLight = 15000.f;
	BladeState = EBladeState::BS_BladeOff;

	BladeMesh = CreateDefaultSubobject<USkeletalMeshComponent>("BladeMesh");
	BladeMesh->SetCastShadow(false);
	SetRootComponent(BladeMesh);
	
	PointLightComponent = CreateDefaultSubobject<UPointLightComponent>("PointLight");
	PointLightComponent->SetupAttachment(BladeMesh);
}

void ABladeBase::BeginPlay()
{
	Super::BeginPlay();

	if(ALightsaberBase* Lightsaber = Cast<ALightsaberBase>(GetParentActor()))
	{
		Lightsaber->InitAudioComponent();
	}

	if(BladeState == EBladeState::BS_BladeOn)
	{
		UpdateBladeAnimation(true);
		PointLightComponent->SetIntensity(MaxIntensityPointLight);
	}
	else
	{
		UpdateBladeAnimation(false);
		PointLightComponent->SetIntensity(0.f);
	}
	
	InitBlade();
}

void ABladeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABladeBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABladeBase, BladeState);
}

void ABladeBase::UpdateBladeColor()
{
	if(ALightsaberBase* Lightsaber = Cast<ALightsaberBase>(GetAttachParentActor()))
	{
        if(UMaterialInstanceDynamic* BladeMaterial = BladeMesh->CreateDynamicMaterialInstance(0))
        {
	        BladeMaterial->SetVectorParameterValue("Color", Lightsaber->BladeColor);

        	PointLightComponent->SetLightColor(Lightsaber->BladeColor);
        }
	}
}

void ABladeBase::InitBlade()
{
	if(ALightsaberBase* Lightsaber = Cast<ALightsaberBase>(GetAttachParentActor()))
	{
		if(GetWorldTimerManager().IsTimerActive(InitHandler))
		{
			GetWorldTimerManager().ClearTimer(InitHandler);	
		}
		
		Lightsaber->UpdateBladeColor.AddDynamic(this, &ABladeBase::UpdateBladeColor);
		UpdateBladeColor();
	}
	else
	{
		GetWorldTimerManager().SetTimer(InitHandler, this, &ABladeBase::InitBlade, .5f);
	}
}
