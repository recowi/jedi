// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponBase.h"

AWeaponBase::AWeaponBase()
{
 	PrimaryActorTick.bCanEverTick = false;
	bReplicates = true;

	WeaponType = EWeaponType::WT_Lightsaber;

	RootSceneComponent = CreateDefaultSubobject<USceneComponent>("RootComponent");
	RootComponent = RootSceneComponent;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("Weapon");
	WeaponMesh->SetupAttachment(RootComponent);

}

void AWeaponBase::BeginPlay()
{
	Super::BeginPlay();
	
}

