// Copyright Epic Games, Inc. All Rights Reserved.

#include "Jedi.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Jedi, "Jedi" );
