// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "Engine/DataTable.h"
#include "GameFramework/GameModeBase.h"
#include "Jedi/Player/LobbyPlayerController.h"

#include "LobbyGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API ALobbyGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	FName ServerName;

	UPROPERTY(BlueprintReadWrite, Replicated)
	int MaxPlayers;

	UPROPERTY(Replicated, BlueprintReadWrite)
	int CurrentPlayers;

	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<FLobbyPlayerInfo> ConnectedPlayers;
	
	UPROPERTY(BlueprintReadWrite)
	bool bCanWeStart;

	UPROPERTY(Replicated, BlueprintReadWrite)
	TArray<class ALobbyPlayerController*> AllPlayerControllers;
	
	UPROPERTY(BlueprintReadWrite)
	FMapInfo CurrentMap;
	
	UPROPERTY(BlueprintReadWrite)
	FGameTimeInfo CurrentGameTime;

	UPROPERTY(BlueprintReadWrite)
	TArray<FMapInfo> AllMapsInfo;
	
	UPROPERTY(BlueprintReadWrite)
	TArray<FGameTimeInfo> AllGameTimeInfo;
	
private:
	UPROPERTY(EditDefaultsOnly)
	UDataTable* MapsDataTable;
	
	UPROPERTY(EditDefaultsOnly)
	UDataTable* GameTimeDataTable;
	
public:
	ALobbyGameModeBase();

protected:
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void BeginPlay() override;
	virtual void PostLogin(APlayerController* NewPlayer) override;
	virtual void Logout(AController* Exiting) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

public:
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void EveryoneUpdate();

	UFUNCTION(BlueprintCallable)
	void StartMatch();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void YouHaveBeenKicked(int PlayerID);

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerUpdateGameSettings(FMapInfo SelectedMap);
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
    void ServerUpdateGameTime(FGameTimeInfo GameTime);

private:
	void LaunchGame();
	
};
