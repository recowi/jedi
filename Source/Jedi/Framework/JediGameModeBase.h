// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "JediGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API AJediGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
