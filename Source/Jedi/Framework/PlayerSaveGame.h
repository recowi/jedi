// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "PlayerSaveGame.generated.h"



USTRUCT(BlueprintType)
struct FPlayerInfo
{
	GENERATED_BODY()
	
	UPROPERTY(BlueprintReadWrite)
	FText PlayerName;
	
	UPROPERTY(BlueprintReadWrite)
	UTexture2D* PlayerAvatar;
	
};



UCLASS()
class JEDI_API UPlayerSaveGame : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(Replicated, EditDefaultsOnly, BlueprintReadWrite, Replicated)
	FPlayerInfo PlayerInfo;

protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
};
