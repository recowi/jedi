// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyGameModeBase.h"



#include "JediGameInstance.h"
#include "Jedi/Player/LobbyPlayerController.h"
#include "Net/UnrealNetwork.h"

ALobbyGameModeBase::ALobbyGameModeBase()
{
	bUseSeamlessTravel = true;
}

void ALobbyGameModeBase::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);

	if(MapsDataTable)
	{
		TArray<FName> AllRowsNames = MapsDataTable->GetRowNames();
		for(auto& RowName : AllRowsNames)
		{
			FMapInfo MapInfo = *MapsDataTable->FindRow<FMapInfo>(RowName, "");
			AllMapsInfo.Add(MapInfo);
		}
		CurrentMap = AllMapsInfo[0];
	}
		
	if(GameTimeDataTable)
	{
		TArray<FName> AllGameTimeRowsNames = GameTimeDataTable->GetRowNames();
		for(auto& RowName : AllGameTimeRowsNames)
		{
			FGameTimeInfo MapInfo = *GameTimeDataTable->FindRow<FGameTimeInfo>(RowName, "");
			AllGameTimeInfo.Add(MapInfo);
		}
		CurrentGameTime = AllGameTimeInfo[0];
	}
}

void ALobbyGameModeBase::BeginPlay()
{
	Super::BeginPlay();

}


void ALobbyGameModeBase::PostLogin(APlayerController* NewPlayer)
{
	Super::PostLogin(NewPlayer);

	if(HasAuthority())
	{
		ALobbyPlayerController* PC = Cast<ALobbyPlayerController>(NewPlayer);
		AllPlayerControllers.Add(PC);

		UJediGameInstance* GameInstance = Cast<UJediGameInstance>(GetGameInstance());

		ServerName = GameInstance->ServerName;
		MaxPlayers = GameInstance->MaxPlayers;
		
		PC->InitialSetup();
		PC->SetupLobbyMenu(ServerName);
		PC->UpdateCurrentMap(CurrentMap);
		PC->UpdateGameTime(CurrentGameTime);

		EveryoneUpdate();
	}
}

void ALobbyGameModeBase::Logout(AController* Exiting)
{
	Super::Logout(Exiting);
	
	if(auto ExitingPC = Cast<ALobbyPlayerController>(Exiting))
	{
		AllPlayerControllers.RemoveSingle(ExitingPC);
		EveryoneUpdate();
	}
}

void ALobbyGameModeBase::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ALobbyGameModeBase, AllPlayerControllers);
	DOREPLIFETIME(ALobbyGameModeBase, ServerName);
	DOREPLIFETIME(ALobbyGameModeBase, MaxPlayers);
	DOREPLIFETIME(ALobbyGameModeBase, CurrentPlayers);
	DOREPLIFETIME(ALobbyGameModeBase, ConnectedPlayers);
	DOREPLIFETIME(ALobbyGameModeBase, ConnectedPlayers);
}

void ALobbyGameModeBase::StartMatch()
{
	for(auto& PlayerController : AllPlayerControllers)
	{
		PlayerController->CloseLobbyMenu();
		PlayerController->ShowLoadingScreen();
	}

	LaunchGame();
}

void ALobbyGameModeBase::YouHaveBeenKicked_Implementation(int PlayerID)
{
	if(AllPlayerControllers.IsValidIndex(PlayerID))
	{
		AllPlayerControllers[PlayerID]->Kicked();
	}
}

void ALobbyGameModeBase::ServerUpdateGameSettings_Implementation(FMapInfo SelectedMap)
{
	CurrentMap = SelectedMap;

	for(auto& PC : AllPlayerControllers)
	{
		PC->UpdateCurrentMap(CurrentMap);
	}
}

void ALobbyGameModeBase::ServerUpdateGameTime_Implementation(FGameTimeInfo GameTime)
{
	CurrentGameTime = GameTime;

	for(auto& PC : AllPlayerControllers)
	{
		PC->UpdateGameTime(CurrentGameTime);
	}
}

void ALobbyGameModeBase::LaunchGame()
{
	GetWorld()->ServerTravel(CurrentMap.URL);
}

void ALobbyGameModeBase::EveryoneUpdate_Implementation()
{
	CurrentPlayers = AllPlayerControllers.Num();

	if(CurrentPlayers > 0)
	{
		ConnectedPlayers.Empty();

		for(auto& PC : AllPlayerControllers)
		{
			ConnectedPlayers.Add(PC->PlayerInfo);
			PC->UpdateNumberOfPlayers(CurrentPlayers, MaxPlayers);
		}

		for(auto& PC : AllPlayerControllers)
		{
			PC->AddPlayerInfo(ConnectedPlayers);
		}

		bCanWeStart = true;
		
		for(auto& PC : AllPlayerControllers)
		{
			if(PC->PlayerInfo.Status.ToString() == "NOT READY")
			{
				bCanWeStart = false;
				break;
			}
		}
	}
}