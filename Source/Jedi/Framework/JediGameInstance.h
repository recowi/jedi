// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "FindSessionsCallbackProxy.h"
#include "NetworkReplayStreaming.h"
#include "OnlineSessionSettings.h"
#include "Engine/GameInstance.h"
#include "Interfaces/OnlineSessionInterface.h"

#include "JediGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API UJediGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<class UMainMenuWidget> MainMenuWidgetClass;
	
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<class UUserWidget> OptionsMenuWidgetClass;
	
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<class UUserWidget> LoadingScreenWidgetClass;
	
	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<class UAcceptMatchWidget> AcceptMatchWidgetClass;

	UPROPERTY(EditDefaultsOnly, Category="Widgets")
	TSubclassOf<class UUserWidget> MatchSearchFailureWidgetClass;

	UPROPERTY(BlueprintReadWrite)
	class UMainMenuWidget* MainMenuWidget;
	
	UPROPERTY(BlueprintReadWrite)
	class UUserWidget* OptionsMenuWidget;
	
	UPROPERTY(BlueprintReadWrite)
	class UUserWidget* LoadingScreenWidget;
	
	UPROPERTY(BlueprintReadWrite)
	class UAcceptMatchWidget* AcceptMatchWidget;
	
	UPROPERTY(BlueprintReadWrite)
	class UUserWidget* MatchSearchFailureWidget;

	UPROPERTY(Replicated, BlueprintReadWrite)
	int MaxPlayers;
	
	UPROPERTY(Replicated, BlueprintReadWrite)
	FName ServerName;
	
	FTimerHandle TimerHandle_AcceptMatch;

	FOnlineSessionSearchResult AvailableSession;

private:
	UPROPERTY(EditDefaultsOnly)
	FName LobbyMapName;

	UPROPERTY(EditDefaultsOnly)
	bool bEnableLan;

	UPROPERTY()
	bool bFindingMatch;

	UPROPERTY(EditDefaultsOnly)
	float FullAcceptTime;
	
	TSharedPtr<class FOnlineSessionSettings> SessionSettings;
	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	/* Delegate called when session created */
	FOnCreateSessionCompleteDelegate OnCreateSessionCompleteDelegate;
	/* Delegate called when session started */
	FOnStartSessionCompleteDelegate OnStartSessionCompleteDelegate;

	/** Handles to registered delegates for creating/starting a session */
	FDelegateHandle OnCreateSessionCompleteDelegateHandle;
	FDelegateHandle OnStartSessionCompleteDelegateHandle;
	
	/** Delegate for joining a session */
	FOnJoinSessionCompleteDelegate OnJoinSessionCompleteDelegate;

	/** Handle to registered delegate for joining a session */
	FDelegateHandle OnJoinSessionCompleteDelegateHandle;

	
	/** Delegate for searching for sessions */
	FOnFindSessionsCompleteDelegate OnFindSessionsCompleteDelegate;

	/** Handle to registered delegate for searching a session */
	FDelegateHandle OnFindSessionsCompleteDelegateHandle;
	

	/** Delegate for destroying a session */
	FOnDestroySessionCompleteDelegate OnDestroySessionCompleteDelegate;

	/** Handle to registered delegate for destroying a session */
	FDelegateHandle OnDestroySessionCompleteDelegateHandle;
	
public:
	UJediGameInstance(const FObjectInitializer& ObjectInitializer);
	
protected:
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	virtual bool JoinSession(ULocalPlayer* LocalPlayer, int32 SessionIndexInSearchResults) override;
	virtual bool JoinSession(ULocalPlayer* LocalPlayer, const FOnlineSessionSearchResult& SearchResult) override;
	
public:
	UFUNCTION(BlueprintCallable)
	void ShowMainMenu();
	
	UFUNCTION(BlueprintCallable)
    void ShowOptionsMenu();
    
	UFUNCTION(BlueprintCallable)
    void ShowLoadingScreen();
	
	UFUNCTION(BlueprintCallable)
    void LaunchLobby(int NumberOfPlayers, FName NewServerName);

	UFUNCTION(BlueprintCallable)
	void FindOnlineGames();

	UFUNCTION(BlueprintCallable)
	void JoinOnlineGame();
	
	UFUNCTION(BlueprintCallable)
    void DestroySession();

	UFUNCTION(BlueprintCallable)
	void StartFindingMatch();

	UFUNCTION(BlueprintCallable)
	void StopFindingMatch();

	UFUNCTION(BlueprintCallable)
	void AcceptJoinSession();

	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetFullTimeAccept() const { return FullAcceptTime; }
	
	UFUNCTION(BlueprintPure)
	FORCEINLINE float GetRemainingTimeAccept() const { return GetWorld()->GetTimerManager().GetTimerRemaining(TimerHandle_AcceptMatch); }
	
private:
	bool HostSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, bool bIsLAN, bool bIsPresence, int32 MaxNumPlayers);
	
	virtual void OnCreateSessionComplete(FName SessionName, bool bWasSuccessful);
	void OnStartOnlineGameComplete(FName SessionName, bool bWasSuccessful);
	
	bool JoinSession(TSharedPtr<const FUniqueNetId> UserId, FName SessionName, const FOnlineSessionSearchResult& SearchResult);
	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);

	void FindSessions(TSharedPtr<const FUniqueNetId> UserId, bool bIsLAN, bool bIsPresence);
	void OnFindSessionsComplete(bool bWasSuccessful);
	
	virtual void OnDestroySessionComplete(FName SessionName, bool bWasSuccessful);

	UFUNCTION()
	void CloseAcceptJoinWidget() const;

	void ShowAcceptMatchWidget();
	
};
