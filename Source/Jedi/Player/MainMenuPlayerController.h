// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Jedi/Framework/PlayerSaveGame.h"

#include "MainMenuPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API AMainMenuPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite)
	FPlayerInfo PlayerInfo;
	
public:
	AMainMenuPlayerController();
	
protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintImplementableEvent)
	void SetupSteamNameAndAvatar();
	
};
