// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerCharacter.h"


#include "Camera/CameraComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Jedi/Weapons/Lightsabers/LightsaberBase.h"
#include "Kismet/KismetMathLibrary.h"
#include "Net/UnrealNetwork.h"

APlayerCharacter::APlayerCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	PlayerSettings.GeneralSensitivity = 30.f;
	PlayerSettings.HorizontalSensitivity = 1.f;
	PlayerSettings.VerticalSensitivity = 1.f;
	PlayerSettings.bNeedHoldSprint = true;
	
	bConsiderReleasedSprint = true;

	MaxWalkSpeed = 168.f;
	MaxSprintSpeed = 450.f;

	Face = CreateDefaultSubobject<USkeletalMeshComponent>("Face");
	Face->SetMasterPoseComponent(GetMesh());
	Face->SetupAttachment(GetMesh());
	
	Torso = CreateDefaultSubobject<USkeletalMeshComponent>("Torso");
	Torso->SetMasterPoseComponent(GetMesh());
	Torso->SetupAttachment(GetMesh());
	
	Legs = CreateDefaultSubobject<USkeletalMeshComponent>("Legs");
	Legs->SetMasterPoseComponent(GetMesh());
	Legs->SetupAttachment(GetMesh());
	
	Feet = CreateDefaultSubobject<USkeletalMeshComponent>("Feet");
	Feet->SetMasterPoseComponent(GetMesh());
	Feet->SetupAttachment(GetMesh());

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	SpringArmComponent->bEnableCameraLag = true;
	SpringArmComponent->bEnableCameraRotationLag = true;
	SpringArmComponent->bUsePawnControlRotation = true;
	SpringArmComponent->SetupAttachment(GetMesh());

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("Camera");
	CameraComponent->SetupAttachment(SpringArmComponent, SpringArmComponent->SocketName);

}

void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();

	SpawnWeapon();
}

void APlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if(IsLocallyControlled())
	{
		CalculateAimOffset();
		CheckSprintDirection();
	}
}

void APlayerCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MoveForward", this, &APlayerCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APlayerCharacter::MoveRight);
	
	PlayerInputComponent->BindAxis("LookUp", this, &APlayerCharacter::LookUp);
	PlayerInputComponent->BindAxis("Turn", this, &APlayerCharacter::Turn);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &APlayerCharacter::PressedSprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &APlayerCharacter::ReleasedSprint);

	PlayerInputComponent->BindAction("ToggleFirstBlade", IE_Pressed, this, &APlayerCharacter::ToggleFirstBlade);
	PlayerInputComponent->BindAction("ToggleSecondBlade", IE_Released, this, &APlayerCharacter::ToggleSecondBlade);

	PlayerInputComponent->BindAction("GetLightsaber", IE_Pressed, this, &APlayerCharacter::GetLightsaber);
	
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(APlayerCharacter, MoveForwardAxis, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(APlayerCharacter, MoveRightAxis, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(APlayerCharacter, YawInput, COND_SkipOwner);
	DOREPLIFETIME_CONDITION(APlayerCharacter, PitchInput, COND_SkipOwner);
	DOREPLIFETIME(APlayerCharacter, MovementState);
	DOREPLIFETIME(APlayerCharacter, Lightsaber);
	DOREPLIFETIME(APlayerCharacter, CurrentWeapon);
}

void APlayerCharacter::ServerUnequipLightsaber_Implementation()
{
	Lightsaber->AttachToComponent
    (
            GetMesh(),
            FAttachmentTransformRules::KeepRelativeTransform, 
            Lightsaber->AttachSocketName
    );

	CurrentWeapon = nullptr;
}

void APlayerCharacter::TurnsFirstBlade()
{
	if(IsLocallyControlled())
	{
		ServerTurnFirstBlade();
	}
}

void APlayerCharacter::TurnOffBlades()
{
	if(IsLocallyControlled())
	{
		ServerTurnOffBlades();
	}
}

void APlayerCharacter::ServerTurnFirstBlade_Implementation()
{
	if(CurrentWeapon == Lightsaber)
	{
		Lightsaber->ToggleFirstBlade();
	}
}

void APlayerCharacter::ServerTurnOffBlades_Implementation()
{
	if(CurrentWeapon == Lightsaber)
	{
		Lightsaber->TurnOffBlades();
	}
}

void APlayerCharacter::MoveForward(float Axis)
{
	if(MoveForwardAxis != Axis)
	{
		SetMoveForwardAxis(Axis);
	}

	AddMovementInput(GetActorForwardVector(), Axis);
}

void APlayerCharacter::MoveRight(float Axis)
{
	if(MoveRightAxis != Axis)
	{
		SetMoveRightAxis(Axis);
	}
	
	AddMovementInput(GetActorRightVector(), Axis);
}

void APlayerCharacter::LookUp(float Axis)
{
	AddControllerPitchInput(Axis * GetVerticalSensitivity() * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::Turn(float Axis)
{
	AddControllerYawInput(Axis * GetHorizontalSensitivity() * GetWorld()->GetDeltaSeconds());
}

void APlayerCharacter::SetMoveForwardAxis(float Axis)
{
	if(!HasAuthority())
	{
		ServerSetMoveForwardAxis(Axis);
	}

	MoveForwardAxis = Axis;
}

void APlayerCharacter::SetMoveRightAxis(float Axis)
{
	if(!HasAuthority())
	{
		ServerSetMoveRightAxis(Axis);
	}

	MoveRightAxis = Axis;
}

void APlayerCharacter::ServerSetMoveRightAxis_Implementation(float Axis)
{
	SetMoveRightAxis(Axis);
}

void APlayerCharacter::ServerSetMoveForwardAxis_Implementation(float Axis)
{
	SetMoveForwardAxis(Axis);
}

void APlayerCharacter::CalculateAimOffset()
{
	const float Pitch = UKismetMathLibrary::NormalizedDeltaRotator(GetControlRotation(), GetActorRotation()).Pitch;
	const float Yaw = GetControlRotation().Yaw;
	
	if(YawInput != Yaw || PitchInput != Pitch)
	{
		SetupYawAndPitch(Yaw, Pitch);
	}
}

void APlayerCharacter::SetupYawAndPitch(const float Yaw, const float Pitch)
{
	if(!HasAuthority())
	{
		ServerSetupYawAndPitch(Yaw, Pitch);
	}
	
	YawInput = Yaw;
	PitchInput = Pitch;
}

void APlayerCharacter::OnRep_MovementState()
{
	switch (MovementState)
	{
		case EMovementState::EMS_Walk:
			StartWalk();
			break;
		case EMovementState::EMS_Sprint:
			StartSprint();
			break;
	}
}

void APlayerCharacter::PressedSprint()
{
	bConsiderReleasedSprint = true;
	ToggleSprint();
}

void APlayerCharacter::ReleasedSprint()
{
	if(PlayerSettings.bNeedHoldSprint && bConsiderReleasedSprint)
	{
		ToggleSprint();
		return;
	}

	if(PlayerSettings.bNeedHoldSprint && !bConsiderReleasedSprint)
	{
		bConsiderReleasedSprint = true;
	}
}

void APlayerCharacter::ToggleSprint()
{
	switch (MovementState)
	{
	case EMovementState::EMS_Sprint:
		ServerSetMovementState(EMovementState::EMS_Walk);
		break;

		case EMovementState::EMS_Walk:
			if(MoveForwardAxis > 0)
			{
			    ServerSetMovementState(EMovementState::EMS_Sprint);
			}
			break;	
	}
}

void APlayerCharacter::StartWalk() const
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
}

void APlayerCharacter::StartSprint() const
{
	GetCharacterMovement()->MaxWalkSpeed = MaxSprintSpeed;
}

void APlayerCharacter::CheckSprintDirection()
{
	if(MoveForwardAxis != 1 && MovementState == EMovementState::EMS_Sprint)
	{
		ToggleSprint();

		if(PlayerSettings.bNeedHoldSprint)
		{
			bConsiderReleasedSprint = false;
		}
	}
	
}

void APlayerCharacter::SpawnWeapon()
{
	if(HasAuthority())
	{
		UWorld* World = GetWorld();

		FActorSpawnParameters WeaponSpawnParameters;
		WeaponSpawnParameters.Owner = this;

		Lightsaber = World->SpawnActor<ALightsaberBase>
		(
			LightsaberClass,
			FVector::ZeroVector,
			FRotator::ZeroRotator,
			WeaponSpawnParameters
		);

		Lightsaber->AttachToComponent
		(
			GetMesh(),
			FAttachmentTransformRules::KeepRelativeTransform, 
			Lightsaber->AttachSocketName
		);

		// CurrentWeapon = Lightsaber;
	}
}

void APlayerCharacter::ToggleFirstBlade()
{
	if(CurrentWeapon == Lightsaber)
	{
		if(!HasAuthority())
		{
			ServerToggleFirstBlade();
			return;
		}

		Lightsaber->ToggleFirstBlade();
	}
}

void APlayerCharacter::ToggleSecondBlade()
{
	if(CurrentWeapon == Lightsaber)
	{
		if(!HasAuthority())
		{
			ServerToggleSecondBlade();
			return;
		}

		Lightsaber->ToggleSecondBlade();
	}
}

void APlayerCharacter::GetLightsaber()
{
	if(CurrentWeapon != Lightsaber)
	{
		ServerStartEquipLightsaber();
	}
	else if(CurrentWeapon == Lightsaber)
	{
		ServerStartUnequipLightsaber();
	}
}

void APlayerCharacter::MultiStartUnequipLightsaber_Implementation()
{
	if(UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
	{
		AnimInstance->Montage_Play(UnequipLightsaberAnim);
	}
}

void APlayerCharacter::ServerStartUnequipLightsaber_Implementation()
{
	MultiStartUnequipLightsaber();
}

void APlayerCharacter::MultiStartEquipLightsaber_Implementation()
{
	if(UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance())
	{
		AnimInstance->Montage_Play(EquipLightsaberAnim);
	}
}

void APlayerCharacter::ServerEquipLightsaber_Implementation()
{
	Lightsaber->AttachToComponent
	(
            GetMesh(),
            FAttachmentTransformRules::KeepRelativeTransform, 
            Lightsaber->AttachHandsSocketName
	);

	CurrentWeapon = Lightsaber;
}

void APlayerCharacter::ServerStartEquipLightsaber_Implementation()
{
	MultiStartEquipLightsaber();	
}

void APlayerCharacter::ServerToggleSecondBlade_Implementation()
{
	ToggleSecondBlade();
}

void APlayerCharacter::ServerToggleFirstBlade_Implementation()
{
	ToggleFirstBlade();
}


void APlayerCharacter::ServerSetMovementState_Implementation(EMovementState NewMovementState)
{
	MovementState = NewMovementState;

	switch (NewMovementState)
	{
		case EMovementState::EMS_Sprint:
			StartSprint();
			break;
		case EMovementState::EMS_Walk:
			StartWalk();
			break;
	}
}

void APlayerCharacter::ServerSetupYawAndPitch_Implementation(const float Yaw, const float Pitch)
{
	SetupYawAndPitch(Yaw, Pitch);
}
