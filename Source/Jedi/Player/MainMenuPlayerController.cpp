// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuPlayerController.h"

AMainMenuPlayerController::AMainMenuPlayerController()
{
}

void AMainMenuPlayerController::BeginPlay()
{
	FTimerHandle TimerHandle;
	
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AMainMenuPlayerController::SetupSteamNameAndAvatar, 0.2f);
}
