// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "PlayerCharacter.generated.h"

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	EMS_Walk UMETA(DisplayName = "Walk"),
	EMS_Sprint UMETA(DisplayName = "Sprint")
};


USTRUCT(BlueprintType)
struct FPlayerControllerSettings
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings")
	float GeneralSensitivity;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings")
    float VerticalSensitivity;
    
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings")
	float HorizontalSensitivity;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Settings")
	bool bNeedHoldSprint;
	
};


UCLASS()
class JEDI_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="Components")
	class USkeletalMeshComponent* Face;
	
	UPROPERTY(EditDefaultsOnly, Category="Components")
	class USkeletalMeshComponent* Torso;

	UPROPERTY(EditDefaultsOnly, Category="Components")
	class USkeletalMeshComponent* Legs;
	
	UPROPERTY(EditDefaultsOnly, Category="Components")
	class USkeletalMeshComponent* Feet;

	UPROPERTY(EditDefaultsOnly, Category="Camera")
	class USpringArmComponent* SpringArmComponent;

	UPROPERTY(EditDefaultsOnly, Category="Camera")
	class UCameraComponent* CameraComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category="PlayerSettings")
	FPlayerControllerSettings PlayerSettings;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* EquipLightsaberAnim;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UAnimMontage* UnequipLightsaberAnim;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	TSubclassOf<class ALightsaberBase> LightsaberClass;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
	class ALightsaberBase* Lightsaber;
	
	UPROPERTY(BlueprintReadWrite, Replicated)
	class AWeaponBase* CurrentWeapon;
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	float MoveForwardAxis;
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	float MoveRightAxis;

	UPROPERTY(BlueprintReadOnly, Replicated)
	float YawInput;
	
	UPROPERTY(BlueprintReadOnly, Replicated)
	float PitchInput;
	
	UPROPERTY(BlueprintReadOnly, ReplicatedUsing=OnRep_MovementState)
	EMovementState MovementState;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxWalkSpeed;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
	float MaxSprintSpeed;

private:
	bool bConsiderReleasedSprint;
	
public:
	APlayerCharacter();

protected:
	virtual void BeginPlay() override;
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
public:
	UFUNCTION(Server, Reliable, BlueprintCallable)
    void ServerEquipLightsaber();
    
	UFUNCTION(Server, Reliable, BlueprintCallable)
    void ServerUnequipLightsaber();
	
	UFUNCTION(BlueprintCallable)
	void TurnsFirstBlade();
	
	UFUNCTION(BlueprintCallable)
    void TurnOffBlades();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
	void ServerTurnFirstBlade();
	
	UFUNCTION(Server, Reliable, BlueprintCallable)
    void ServerTurnOffBlades();
	
private:
	void MoveForward(float Axis);
	void MoveRight(float Axis);

	void LookUp(float Axis);
	void Turn(float Axis);

	FORCEINLINE float GetVerticalSensitivity() const { return PlayerSettings.GeneralSensitivity * PlayerSettings.VerticalSensitivity; }
	FORCEINLINE float GetHorizontalSensitivity() const { return PlayerSettings.GeneralSensitivity * PlayerSettings.HorizontalSensitivity; }

	void SetMoveForwardAxis(float Axis);
	void SetMoveRightAxis(float Axis);

	UFUNCTION(Server, Reliable)
	void ServerSetMoveForwardAxis(float Axis);
	
	UFUNCTION(Server, Reliable)
	void ServerSetMoveRightAxis(float Axis);

	void CalculateAimOffset();
	void SetupYawAndPitch(const float Yaw, const float Pitch);

	UFUNCTION(Server, Reliable)
	void ServerSetupYawAndPitch(const float Yaw, const float Pitch);

	UFUNCTION()
	void OnRep_MovementState();

	void PressedSprint();
	void ReleasedSprint();

	void ToggleSprint();

	UFUNCTION(Server, Reliable)
	void ServerSetMovementState(EMovementState NewMovementState);

	void StartWalk() const;
	void StartSprint() const;

	void CheckSprintDirection();

	void SpawnWeapon();

	void ToggleFirstBlade();
	void ToggleSecondBlade();

	UFUNCTION(Server, Reliable)
	void ServerToggleFirstBlade();
	
	UFUNCTION(Server, Reliable)
	void ServerToggleSecondBlade();

	void GetLightsaber();
	
	UFUNCTION(Server, Reliable)
    void ServerStartEquipLightsaber();

	UFUNCTION(NetMulticast, Reliable)
	void MultiStartEquipLightsaber();

	UFUNCTION(Server, Reliable)
    void ServerStartUnequipLightsaber();

	UFUNCTION(NetMulticast, Reliable)
    void MultiStartUnequipLightsaber();
	
};
