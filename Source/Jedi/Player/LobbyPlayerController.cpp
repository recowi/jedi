// Fill out your copyright notice in the Description page of Project Settings.


#include "LobbyPlayerController.h"



#include "Blueprint/UserWidget.h"
#include "Jedi/Framework/JediGameInstance.h"
#include "Jedi/Framework/LobbyGameModeBase.h"
#include "Jedi/Widgets/Lobby/LobbyMenuWidget.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"

ALobbyPlayerController::ALobbyPlayerController()
{
	PlayerInfo.PlayerInfo.PlayerName = FText::FromString("Player");
	PlayerInfo.Status = FText::FromString("NOT READY");
}

void ALobbyPlayerController::BeginPlay()
{
	Super::BeginPlay();
}

void ALobbyPlayerController::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Super::EndPlay(EndPlayReason);
	
}

void ALobbyPlayerController::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ALobbyPlayerController, PlayerInfo);
}

void ALobbyPlayerController::CloseSession_Implementation()
{
	ShowLoadingScreen();
	UGameplayStatics::OpenLevel(GetWorld(), MainMenuMapName);
	
	if(UJediGameInstance* GameInstance = Cast<UJediGameInstance>(GetGameInstance()))
	{
		GameInstance->DestroySession();
	}
}

void ALobbyPlayerController::Kicked_Implementation()
{
	ShowLoadingScreen();
	UGameplayStatics::OpenLevel(GetWorld(), MainMenuMapName);
	
}

void ALobbyPlayerController::InitialSetup_Implementation()
{
	GameMode = Cast<ALobbyGameModeBase>(GetWorld()->GetAuthGameMode());
	
	SetupPlayerInfo();
}

void ALobbyPlayerController::UpdateGameTime_Implementation(FGameTimeInfo GameTime)
{
	if(LobbyMenuWidget)
	{
		LobbyMenuWidget->CurrentGameTime = GameTime;
	}
}

void ALobbyPlayerController::CloseLobbyMenu_Implementation()
{
	if(LobbyMenuWidget)
	{
		LobbyMenuWidget->RemoveFromParent();
	}
}

void ALobbyPlayerController::ShowLoadingScreen_Implementation()
{
	if(auto GameInstance = Cast<UJediGameInstance>(GetGameInstance()))
	{
		GameInstance->ShowLoadingScreen();
	}
}

void ALobbyPlayerController::AddPlayerInfo_Implementation(const TArray<FLobbyPlayerInfo>& ConnectedPlayersInfo)
{
	if(LobbyMenuWidget)
	{
		LobbyMenuWidget->ClearPlayerList();

		for(auto& ConnectedPlayerInfo : ConnectedPlayersInfo)
		{
			LobbyMenuWidget->UpdatePlayerWindow(ConnectedPlayerInfo);
		}
	}
}

void ALobbyPlayerController::SetupLobbyMenu_Implementation(const FName ServerName)
{
	SetShowMouseCursor(true);

	LobbyMenuWidget = CreateWidget<ULobbyMenuWidget>(this, LobbyMenuWidgetClass);
	LobbyMenuWidget->AddToViewport();
	LobbyMenuWidget->LobbyServerName = ServerName;
}

void ALobbyPlayerController::ToggleReady_Implementation()
{	
	if(PlayerInfo.Status.ToString() == "HOST")
	{
		GameMode->StartMatch();
		return;
	}

	if(PlayerInfo.Status.ToString() == "READY")
	{
		PlayerInfo.Status = FText::FromString("NOT READY");
	}
	else
	{
		PlayerInfo.Status = FText::FromString("READY");
	}
	
	GameMode->EveryoneUpdate();
}

void ALobbyPlayerController::UpdateNumberOfPlayers_Implementation(const int CurrentPlayersNum, const int MaxPlayersNum) const
{
	if(LobbyMenuWidget)
	{
		LobbyMenuWidget->CurrentPlayersNum = CurrentPlayersNum;
		LobbyMenuWidget->MaxPlayersNum = MaxPlayersNum;
	}
}

void ALobbyPlayerController::UpdateCurrentMap_Implementation(FMapInfo MapInfo)
{
	if(LobbyMenuWidget)
	{
		LobbyMenuWidget->CurrentMapInfo = MapInfo;
	}
}

void ALobbyPlayerController::CallUpdate_Implementation(FLobbyPlayerInfo NewPlayerInfo, bool bNewIsReady)
{
	PlayerInfo = NewPlayerInfo;
	
	GameMode->EveryoneUpdate();
}


