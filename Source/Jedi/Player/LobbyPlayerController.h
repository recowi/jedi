// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"


#include "Engine/DataTable.h"
#include "GameFramework/PlayerController.h"
#include "Jedi/Framework/PlayerSaveGame.h"

#include "LobbyPlayerController.generated.h"


USTRUCT(BlueprintType)
struct FGameTimeInfo : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int Time;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FText TimeText;
	
};


USTRUCT(BlueprintType)
struct FMapInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FText MapName;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	UTexture2D* MapImage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	FString URL;

};

USTRUCT(BlueprintType)
struct FLobbyPlayerInfo
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FPlayerInfo PlayerInfo;

	UPROPERTY(BlueprintReadWrite)
	FText Status;
	
};

/**
 * 
 */
UCLASS()
class JEDI_API ALobbyPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintReadWrite, Replicated)
	FLobbyPlayerInfo PlayerInfo;
	
	UPROPERTY(BlueprintReadWrite)
	class ALobbyGameModeBase* GameMode;

	UPROPERTY(BlueprintReadWrite)
	class ULobbyMenuWidget* LobbyMenuWidget;
	
private:
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<UUserWidget> LobbyMenuWidgetClass;

	UPROPERTY(EditDefaultsOnly)
	FName MainMenuMapName;

	UPROPERTY(Replicated)
	TArray<FLobbyPlayerInfo> AllConnectedPlayersInfo;

public:
	ALobbyPlayerController();

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;
	
public:
	UFUNCTION(BlueprintImplementableEvent)
    void SetupPlayerInfo();
	
	UFUNCTION(Client, Reliable, BlueprintCallable)
	void Kicked();

	UFUNCTION(Client, Reliable, BlueprintCallable)
	void CloseSession();

	UFUNCTION(Server, Reliable, BlueprintCallable)
    void ToggleReady();

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void CallUpdate(FLobbyPlayerInfo NewPlayerInfo, bool bNewIsReady);

	UFUNCTION(Server, Reliable)
	void InitialSetup();

	UFUNCTION(Client, Reliable)
	void SetupLobbyMenu(const FName ServerName);

	UFUNCTION(Client, Reliable)
	void ShowLoadingScreen();
	
	UFUNCTION(Client, Reliable)
	void UpdateCurrentMap(FMapInfo MapInfo);

	UFUNCTION(Client, Reliable)
	void UpdateGameTime(FGameTimeInfo GameTime);

	UFUNCTION(Client, Reliable)
	void UpdateNumberOfPlayers(const int CurrentPlayersNum, const int MaxPlayersNum) const;

	UFUNCTION(Client, Reliable)
	void AddPlayerInfo(const TArray<FLobbyPlayerInfo>& ConnectedPlayersInfo);

	UFUNCTION(Client, Reliable)
	void CloseLobbyMenu();
	
};
