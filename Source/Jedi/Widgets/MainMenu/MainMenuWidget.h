// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MainMenuWidget.generated.h"

UENUM(BlueprintType)
enum class EMenuItems : uint8 
{
	EMI_Game UMETA(DisplayName = "Game"),
	EMI_Character UMETA(DisplayName = "Character"),
	EMI_Options UMETA(DisplayName = "Options"),
};


UCLASS()
class JEDI_API UMainMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	class UJediGameInstance* GameInstance;

	UPROPERTY(BlueprintReadWrite)
	bool bFindingMatch;

	UPROPERTY(BlueprintReadWrite)
	EMenuItems SelectedMenuItem;

public:
	UMainMenuWidget(const FObjectInitializer& ObjectInitializer);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void CreateGameClicked();
	
	UFUNCTION(BlueprintCallable)
    void FindMatchClicked();
    
	UFUNCTION(BlueprintCallable)
    void OptionsClicked();
	
	UFUNCTION(BlueprintCallable)
    void GameClicked();
	
	UFUNCTION(BlueprintCallable)
    void CharacterClicked();

	UFUNCTION(BlueprintCallable)
    void QuitClicked();

	UFUNCTION(BlueprintCallable)
	void StopFindingMatch();

	UFUNCTION(BlueprintImplementableEvent)
	void PlayStarFindingMatchAnimation();

	UFUNCTION(BlueprintImplementableEvent)
	void ChangeActiveWidgetInSwitcher();

};
