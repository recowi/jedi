// Fill out your copyright notice in the Description page of Project Settings.


#include "AcceptMatchWidget.h"

#include "Jedi/Framework/JediGameInstance.h"

UAcceptMatchWidget::UAcceptMatchWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	GameInstance = Cast<UJediGameInstance>(GetGameInstance());
}
