// Fill out your copyright notice in the Description page of Project Settings.


#include "HostMenuWidget.h"

#include "Jedi/Framework/JediGameInstance.h"

UHostMenuWidget::UHostMenuWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	MaxPlayers = 10;
	NumberOfPlayers = 2;
}

void UHostMenuWidget::AcceptClicked()
{
	BackClicked();
	
	if(UJediGameInstance* GameInstance = Cast<UJediGameInstance>(GetGameInstance()))
	{
		GameInstance->LaunchLobby(NumberOfPlayers, ServerNameText);
	}
}

void UHostMenuWidget::SubtractClicked()
{
	NumberOfPlayers = FMath::Clamp(NumberOfPlayers - 1, 2, MaxPlayers);
}

void UHostMenuWidget::AddClicked()
{	
	NumberOfPlayers = FMath::Clamp(NumberOfPlayers + 1, 2, MaxPlayers);
}
