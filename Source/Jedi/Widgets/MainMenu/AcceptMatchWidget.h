// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Jedi/Framework/JediGameInstance.h"

#include "AcceptMatchWidget.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API UAcceptMatchWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	class UJediGameInstance* GameInstance;

public:
	UAcceptMatchWidget(const FObjectInitializer& ObjectInitializer);
	
};
