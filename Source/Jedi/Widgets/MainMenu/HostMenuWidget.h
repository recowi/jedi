// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "HostMenuWidget.generated.h"

/**
 * 
 */
UCLASS()
class JEDI_API UHostMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite, Category="ServerSettings")
	FName ServerNameText;
	
	UPROPERTY(BlueprintReadWrite, Category="ServerSettings")
	int NumberOfPlayers;

private:
	UPROPERTY(EditDefaultsOnly)
	int MaxPlayers;
	
public:
	UHostMenuWidget(const FObjectInitializer& ObjectInitializer);
	
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void BackClicked();

	UFUNCTION(BlueprintCallable)
	void AcceptClicked();
	
	UFUNCTION(BlueprintCallable)
    void SubtractClicked();
	
	UFUNCTION(BlueprintCallable)
    void AddClicked();
	
};
