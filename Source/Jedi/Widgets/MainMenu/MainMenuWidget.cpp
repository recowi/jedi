// Fill out your copyright notice in the Description page of Project Settings.


#include "MainMenuWidget.h"

#include "Jedi/Framework/JediGameInstance.h"

UMainMenuWidget::UMainMenuWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	bFindingMatch = false;
	GameInstance = Cast<UJediGameInstance>(GetGameInstance());

	SelectedMenuItem = EMenuItems::EMI_Game;
}

void UMainMenuWidget::FindMatchClicked()
{
	PlayStarFindingMatchAnimation();
	bFindingMatch = true;

	GameInstance->StartFindingMatch();
}

void UMainMenuWidget::OptionsClicked()
{
	SelectedMenuItem = EMenuItems::EMI_Options;
	ChangeActiveWidgetInSwitcher();
}

void UMainMenuWidget::GameClicked()
{
	SelectedMenuItem = EMenuItems::EMI_Game;
	ChangeActiveWidgetInSwitcher();
}

void UMainMenuWidget::CharacterClicked()
{
	SelectedMenuItem = EMenuItems::EMI_Character;
	ChangeActiveWidgetInSwitcher();
}

void UMainMenuWidget::QuitClicked()
{
	FGenericPlatformMisc::RequestExit(false);
}

void UMainMenuWidget::StopFindingMatch()
{
	bFindingMatch = false;

	GameInstance->StopFindingMatch();
}
