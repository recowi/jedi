// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "Jedi/Player/LobbyPlayerController.h"


#include "LobbyMenuWidget.generated.h"

UCLASS()
class JEDI_API ULobbyMenuWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadWrite)
	FName LobbyServerName;

	UPROPERTY(BlueprintReadWrite)
	FMapInfo CurrentMapInfo;

	UPROPERTY(BlueprintReadWrite)
	FGameTimeInfo CurrentGameTime;

	UPROPERTY(BlueprintReadWrite)
	int CurrentPlayersNum;
	
	UPROPERTY(BlueprintReadWrite)
	int MaxPlayersNum;

public:
	ULobbyMenuWidget(const FObjectInitializer& ObjectInitializer);

public:
	UFUNCTION(BlueprintImplementableEvent)
	void ClearPlayerList();

	UFUNCTION(BlueprintImplementableEvent)
	void UpdatePlayerWindow(FLobbyPlayerInfo ConnectedPlayer);
	
};
